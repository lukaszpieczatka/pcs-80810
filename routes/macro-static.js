module.exports = function (app, addon) {

    let content = `
    <table data-layout="default" ac:local-id="f029fdfd-c17b-4650-852a-00b11ed3ddd7">
      <colgroup>
        <col style="width: 244.0px;" />
        <col style="width: 241.0px;" />
        <col style="width: 241.0px;" />
      </colgroup>
      <tbody>
        <tr>
          <th>
            <p />
          </th>
          <th>
            <p />
          </th>
          <th>
            <p />
          </th>
        </tr>
        <tr>
          <td>
            <ac:structured-macro ac:name="profile" ac:schema-version="1" data-layout="default" ac:local-id="94d93a41-ae28-4729-84f5-f261442ef763" ac:macro-id="b4528046806a8faa1bc088d16596c5e3">
              <ac:parameter ac:name="user">
                <ri:user ri:userkey="8a7f808a7e76c25c017e7814ec6300b8" />
              </ac:parameter>
            </ac:structured-macro>
          </td>
          <td>
            <ac:structured-macro ac:name="profile" ac:schema-version="1" data-layout="default" ac:local-id="a4adce02-a12f-4bd6-aadd-735d16fef0a0" ac:macro-id="268d1856-1838-4c7d-9581-aa3073840e27">
              <ac:parameter ac:name="user">
                <ri:user ri:userkey="8a7f808a7e76c25c017e7814ec6300b8" />
              </ac:parameter>
            </ac:structured-macro>
          </td>
          <td>
            <ac:structured-macro ac:name="profile" ac:schema-version="1" data-layout="default" ac:local-id="25802e55-6fc1-4a27-84ff-c3b05badf79b" ac:macro-id="5a7f5a19-d395-4dae-bf7e-57094d79fab6">
              <ac:parameter ac:name="user">
                <ri:user ri:userkey="8a7f808a7e76c25c017e7814ec6300b8" />
              </ac:parameter>
            </ac:structured-macro>
          </td>
        </tr>
        <tr>
          <td>
            <p />
          </td>
          <td>
            <p />
          </td>
          <td>
            <p />
          </td>
        </tr>
      </tbody>
    </table>
    `;

    // Render the macro by returning a hello world html message
    app.get('/macro-static', addon.authenticate(), function (req, res) {
            res.send(content);
        }
    );

};
